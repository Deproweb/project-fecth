import {Routes,Route} from "react-router-dom";
import { HomePage } from './pages/HomePage/HomePage';
import GalleryInput from './pages/GalleryInput/GalleryInput';
import './App.scss';


function App() {

  return (
    <div className="App">
        <Routes>
          <Route path="/" element={< HomePage/>}/>
          <Route path="gallery" element={<GalleryInput />}/>
        </Routes>
    </div>
  );
}

export default App;




