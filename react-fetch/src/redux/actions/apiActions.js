import axios from "axios";

export const GET_INFO = "[API] getInfo";

const getInfo = (data) => ({
    type: GET_INFO,
    payload: data
})

export const callToApi = (url) => {
    return async (dispatch) => {
        const res = await axios.get(url);
        const data = res.data
        dispatch(getInfo(data))
        console.log(data);
    }}

    /*
    Enlaces para prueba:

    CHEAPSHARK --> https://www.cheapshark.com/api/1.0/deals?storeID=1&upperPrice=15
    POKEMON --> https://pokeapi.co/api/v2/pokemon/ditto
    */