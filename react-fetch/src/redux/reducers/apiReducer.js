import { GET_INFO } from "../actions/apiActions";

const INITIAL_STATE = {
    data: [],
}

export const apiReducer = (state = INITIAL_STATE, action ) => {

    switch (action.type) {

        case GET_INFO:
            return {
                ...state,
                data: action.payload
            }
        default:
            return state
    }
}