import { useState } from 'react';
import { connect} from 'react-redux';
import { callToApi } from '../../redux/actions/apiActions';
import './GalleryInput.scss';

const INITIAL_FORM_STATE = {
    url: '',
};

const GalleryInput = ({dispatch, data}) =>{
    const [formData, setFormData] = useState(INITIAL_FORM_STATE);

    const inputChange = (ev) =>{
        const {name, value} = ev.target;
        setFormData({...formData, [name]: value})
    }
    const submitForm = (ev) =>{
        ev.preventDefault();
        console.log(formData);
        dispatch(callToApi(formData.url));
    }

    return (
        <form onSubmit={submitForm}>
            <label>
                <p>URL</p>
                <input type="url" name="url" id="url" onChange={inputChange}></input>
            </label>
            <div>
                <button>Enviar URL</button>
            </div>
        </form>
    )
}

const mapStateToProps = (state) =>({
    data: state.data,
})
export default connect(mapStateToProps)(GalleryInput);